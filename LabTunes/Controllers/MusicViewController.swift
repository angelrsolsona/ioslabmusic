//
//  MusicViewController.swift
//  LabTunes
//
//  Created by Angel Ricardo Solsona Nevero on 10/11/18.
//  Copyright © 2018 Angel Ricardo Solsona Nevero. All rights reserved.
//

import UIKit

class MusicViewController: UIViewController {

    var songs:[Song] = []
    let searchController = UISearchController(searchResultsController: nil)
    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        downloadSongs()
        setupSerachBar()
        // Do any additional setup after loading the view.
    }
    
    func downloadSongsBy(name: String){
        Music.fetchSongs(songName: name) { (results) in
            self.songs = results
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    func downloadSongs(){
        Music.fetchSongs{ (results) in
            self.songs = results
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    func setupSerachBar(){
        searchController.searchResultsUpdater = self
        searchController.searchBar.placeholder = "Search songs"
        navigationItem.searchController =  searchController
        definesPresentationContext = true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension MusicViewController:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return songs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        let song = songs[indexPath.row]
        cell.textLabel?.text = song.name
        return cell

    }
    
    
}

extension MusicViewController:UISearchResultsUpdating{
    func updateSearchResults(for searchController: UISearchController) {
        downloadSongsBy(name: searchController.searchBar.text!)
    }
    
    
}
