//
//  ViewController.swift
//  LabTunes
//
//  Created by Angel Ricardo Solsona Nevero on 09/11/18.
//  Copyright © 2018 Angel Ricardo Solsona Nevero. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var usuario: UITextField!
    @IBOutlet weak var password: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @IBAction func Login(){
        
        if User.login(userName: usuario.text!, password: password.text!){
            self.performSegue(withIdentifier: "songs_segue", sender: nil)
        }
    }


}

