//
//  Music.swift
//  LabTunes
//
//  Created by Angel Ricardo Solsona Nevero on 09/11/18.
//  Copyright © 2018 Angel Ricardo Solsona Nevero. All rights reserved.
//

import Foundation

class Music {
    static var urlSession = URLSession(configuration: .default)
    
    static func fetchSongs(songName: String = "Queen",onSuccess: @escaping ([Song]) -> Void){

        let url =  URL(string: "https://itunes.apple.com/search?media=music&entity=song&term=\(songName)")
        
        let dataTask = urlSession.dataTask(with: url!) { (data, response, error) in
            if let error = error{
               
            }else if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                if statusCode == 200{
                    guard let json = parseData(data: data!) else {return}
                    let songs = songFrom(json: json)
                    onSuccess(songs)
    
                }
            }
        }
        dataTask.resume()
        

    }
    
    static func parseData(data: Data) -> NSDictionary? {
        let json =  try! JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary
        return json
    }
    
    static func songFrom(json:NSDictionary) -> [Song] {
        let result = json["results"] as! [NSDictionary]
        var songs: [Song] = []
        for dataResult in result{
            let song =  Song.create(dict: dataResult)
            songs.append(song!)
        }
        return songs
        
    }
}
