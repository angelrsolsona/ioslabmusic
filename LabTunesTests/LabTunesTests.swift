//
//  LabTunesTests.swift
//  LabTunesTests
//
//  Created by Angel Ricardo Solsona Nevero on 09/11/18.
//  Copyright © 2018 Angel Ricardo Solsona Nevero. All rights reserved.
//

import XCTest
@testable import LabTunes

class LabTunesTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        let session = Session.sharedInstance
        session.token = nil
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        
    }
    
    func testCorrectLogin(){
        XCTAssertTrue(User.login(userName: "iOSLab", password: "123456"))
    }
    
    func testWrongLogin(){
        XCTAssertFalse(User.login(userName: "iOSLab1", password: "123456"), "Fallo la prueba")
    }
    
    func testSaveSession(){
        let session = Session.sharedInstance
        let _ = User.login(userName: "iOSLab", password: "123456")
        XCTAssertNotNil(session.token)
    }
    
    func testNotSaveSession(){
        let session = Session.sharedInstance
        let _ = User.login(userName: "iOSLab1", password: "123456")
        XCTAssertNil(session.token)
    }
    
    func testExpectedToken(){
        let _ = User.login(userName: "iOSLab", password: "123456")
        let session = Session.sharedInstance
        XCTAssertEqual(session.token, "123456","Token Should Match")
    }
    func testNotExpectedToken(){
        let _ = User.login(userName: "iOSLab2", password: "123456")
        let session = Session.sharedInstance
        XCTAssertNotEqual(session.token, "123456","Token Should Match")
    }
    
    func testFetchSongsWithoutSession(){
        let _ = XCTAssertThrowsError(try User.fetchSongs())
    }
    
    func testMusicSongs(){
        var resultSongs: [Song] = []
        let promise = expectation(description: "Songs Download")
        Music.fetchSongs { (songs) in
            resultSongs = songs
            promise.fulfill()
        }
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertNotEqual(resultSongs.count, 0, "No regresa canciones")
    }
    
    

}
